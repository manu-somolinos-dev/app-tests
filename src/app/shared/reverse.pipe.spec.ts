import {ReversePipe} from './reverse.pipe';

describe('Pipe: ReversePipe', () => { // isolated test: only depends on its own declarations
  it('should work pipe as expected', () => {
    let reversePipe = new ReversePipe();
    expect(reversePipe.transform('hello')).toEqual('olleh');
  });
});
